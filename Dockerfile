# PHPUnit Docker Container.
FROM composer:latest as composer
WORKDIR /composer
COPY composer.json composer.lock ./
RUN composer install --no-scripts --no-progress --ignore-platform-reqs

FROM php:7.4-cli
LABEL mantainer="Alysson Azevedo <alysson@clinicarx.com.br>"

VOLUME ["/app"]
WORKDIR /app

RUN apt-get update \
    && apt-get install -y apt-utils zip zlib1g-dev libicu-dev g++ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl \
    && pecl install xdebug-3.1.1 \
    && docker-php-ext-enable xdebug

COPY xdebug.ini "${PHP_INI_DIR}/conf.d/docker-php-ext-xdebug.ini"
COPY . .
COPY --from=composer /composer/vendor /app/vendor
COPY --from=composer /usr/bin/composer /usr/bin/composer
CMD /app/vendor/bin/phpunit -c phpunit.xml.dist --testdox tests
