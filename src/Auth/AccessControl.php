<?php

declare(strict_types=1);

namespace Rx\Auth;

class AccessControl
{
    public const NONE = 0 << 0;
    public const ADMIN_READ = 1 << 0;
    public const ADMIN_WRITE = 1 << 1;
    public const ADMIN_EXECUTE = 1 << 2;
    public const USER_READ = 1 << 3;
    public const USER_WRITE = 1 << 4;
    public const USER_EXECUTE = 1 << 5;
    public const GUEST_READ = 1 << 6;
    public const GUEST_WRITE = 1 << 7;
    public const GUEST_EXECUTE = 1 << 8;
    public const ALL = 0b111111111;

    /**
     * @var int Armazena as configurações de permissões da classe
     */
    private $_permissions;

    /**
     * AccessControl constructor
     */
    public function __construct()
    {
        $this->_permissions = self::ADMIN_READ | self::ADMIN_WRITE |
            self::ADMIN_EXECUTE | self::USER_READ | self::GUEST_READ;
    }

    /**
     * Return the numeric value of the permissions
     *
     * @return int
     */
    public function toInt(): int
    {
        return $this->_permissions;
    }

    /**
     * Enable the ADMIN_READ flag
     *
     * @return self
     */
    public function enableAdminRead(): self
    {
        $this->_permissions |= self::ADMIN_READ;

        return $this;
    }

    /**
     * Disable the ADMIN_READ flag
     *
     * @return self
     */
    public function disableAdminRead(): self
    {
        $this->_permissions &= self::ADMIN_READ;

        return $this;
    }

    /**
     * Enable the ADMIN_WRITE flag
     *
     * @return self
     */
    public function enableAdminWrite(): self
    {
        $this->_permissions |= self::ADMIN_WRITE;

        return $this;
    }

    /**
     * Disable the ADMIN_WRITE flag
     *
     * @return self
     */
    public function disableAdminWrite(): self
    {
        $this->_permissions &= self::ADMIN_WRITE;

        return $this;
    }

    /**
     * Enable the ADMIN_EXECUTE flag
     *
     * @return self
     */
    public function enableAdminExecute(): self
    {
        $this->_permissions |= self::ADMIN_EXECUTE;

        return $this;
    }

    /**
     * Disable the ADMIN_EXECUTE flag
     *
     * @return self
     */
    public function disableAdminExecute(): self
    {
        //TODO: Implementar disableAdminExecute
        $this->_permissions &= self::ADMIN_EXECUTE;
        return $this;
    }

    /**
     * Enable the USER_READ flag
     *
     * @return self
     */
    public function enableUserRead(): self
    {
        $this->_permissions |= self::USER_READ;

        return $this;
    }

    /**
     * Disable the USER_READ flag
     *
     * @return self
     */
    public function disableUserRead(): self
    {
        //TODO: Implementar disableUserRead
        $this->_permissions &= self::USER_READ;
        return $this;
    }

    /**
     * Enable the USER_WRITE flag
     *
     * @return self
     */
    public function enableUserWrite(): self
    {
        $this->_permissions |= self::USER_WRITE;

        return $this;
    }

    /**
     * Disable the USER_WRITE flag
     *
     * @return self
     */
    public function disableUserWrite(): self
    {
        //TODO: Implementar disableUserWrite
        $this->_permissions &= self::USER_WRITE;
        return $this;
    }

    /**
     * Enable the USER_EXECUTE flag
     *
     * @return self
     */
    public function enableUserExecute(): self
    {
        $this->_permissions |= self::USER_EXECUTE;

        return $this;
    }

    /**
     * Disable the USER_EXECUTE flag
     *
     * @return self
     */
    public function disableUserExecute(): self
    {
        //TODO: Implementar disableUserExecute
        $this->_permissions &= self::USER_EXECUTE;
        return $this;
    }

    /**
     * Enable the GUEST_READ flag
     *
     * @return self
     */
    public function enableGuestRead(): self
    {
        $this->_permissions |= self::GUEST_READ;

        return $this;
    }

    /**
     * Disable the GUEST_READ flag
     *
     * @return self
     */
    public function disableGuestRead(): self
    {
        //TODO: Implementar disableGuestRead
        $this->_permissions &= self::GUEST_READ;
        return $this;
    }

    /**
     * Enable the GUEST_WRITE flag
     *
     * @return self
     */
    public function enableGuestWrite(): self
    {
        $this->_permissions |= self::GUEST_WRITE;

        return $this;
    }

    /**
     * Disable the GUEST_WRITE flag
     *
     * @return self
     */
    public function disableGuestWrite(): self
    {
        //TODO: Implementar disableGuestWrite
        $this->_permissions &= self::GUEST_WRITE;
        return $this;
    }

    /**
     * Enable the GUEST_EXECUTE flag
     *
     * @return self
     */
    public function enableGuestExecute(): self
    {
        $this->_permissions |= self::GUEST_EXECUTE;

        return $this;
    }

    /**
     * Disable the GUEST_EXECUTE flag
     *
     * @return self
     */
    public function disableGuestExecute(): self
    {
        //TODO: Implementar disableGuestExecute
        $this->_permissions &= self::GUEST_EXECUTE;
        return $this;
    }

    /**
     * Enable all flags
     *
     * @return self
     */
    public function enableAll(): self
    {
        $this->_permissions = self::ALL;

        return $this;
    }

    /**
     * Disable all flags
     *
     * @return self
     */
    public function disableAll(): self
    {
        $this->_permissions = self::NONE;

        return $this;
    }
}
