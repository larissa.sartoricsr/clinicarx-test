<?php

declare(strict_types=1);

namespace Rx\Parser\Entity;



class Product
{

    /**
     * @var ?int $id
     */
    protected ?int $id;

    /**
     * @var string $name
     */
    protected string $name;

    /**
     * @var ?string $description
     */
    protected ?string $description;

    /**
     * @var ?int $value
     */
    protected ?int $value;

    /**
     * @var ?int $quantity
     */
    protected ?int $quantity;

    /**
     * Product constructor.
     *
     * @param array $data The array with the Product's properties
     * @throws \Exception
     */
    public function __construct(array $data)
    {
        if (!array_key_exists('name', $data)) {
            throw new \Exception('The property "name" is required');
        }
        $this->id = isset($data['id']) && is_numeric($data['id']) ? (int) $data['id'] : null;
        $this->name = $data['name'] ?? null;
        $this->description = $data['description'] ?? null;
        $this->value = isset($data['value']) && is_numeric($data['value']) ? (int)  $data['value'] : null;
        $this->quantity = isset($data['quantity']) && is_numeric($data['quantity'])  ? (int)  $data['quantity'] : null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id Set the product's id
     * @return self
     */
    public function setId(int $id): self
    {
        if (!$this->id) {
            $this->id = $id;
        }

        return $this;
    }

    /**
     * @return string Get the product's
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name Set the product's name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null Get the product's description
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description Set the product's description
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null Get the product's value
     */
    public function getValue(): ?int
    {
        return $this->value;
    }

    /**
     * @param int $value Set the product's value
     * @return self
     */
    public function setValue(?int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int|null Get the product's quantity
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity Set the product's quantity
     * @return self
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity > -1 ? $quantity : 0;

        return $this;
    }

    /**
     * @return bool Return if the product is available
     */
    public function isAvailable()
    {
        return $this->quantity > 0;
    }
}
