<?php
declare(strict_types=1);

namespace Rx\Parser;

/**
 * Class Collection
 *
 * @package Rx\Parser
 */
class Collection implements \Countable
{
    /**
     * @var array $_container Container de dados da classe
     */
    protected $_container = [];

    /**
     * @var string A classe da entidade usada para decompor os dados
     */
    protected $_entityClass;

    /**
     * Collection constructor.
     *
     * @param array $data The array with the objects that will be stored
     * @param string $entityClass The object's class name. Use `Example::class`.
     * @throws \Exception
     */
    public function __construct(array $data, string $entityClass)
    {
        $this->_entityClass = $entityClass;
        $dictionary = $this->getDictionary($entityClass);

        if (!$data) {
            return;
        }

        $headers = $data[0];
        if ($headers === $dictionary) {
            array_shift($data);
        }
        $this->_container = array_map(
            function ($item) use ($entityClass, $dictionary) {
                if (!$item instanceof $entityClass) {
                    $entityData = array_combine($dictionary, $item);

                    return new $entityClass($entityData);
                } else {
                    return $item;
                }
            },
            $data
        );
    }

    /**
     * @param string $entityClass The class that will be reflected
     * @return array The class properties
     */
    public static function getDictionary(string $entityClass): array
    {
        try {
            $reflection = new \ReflectionClass($entityClass);
            $dictionary = array_map(
                function ($prop) {
                    /** @var \ReflectionProperty $prop */
                    return $prop->getName();
                },
                $reflection->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PROTECTED)
            );
        } catch (\ReflectionException $e) {
            $message = sprintf('Class "%s" does not exists', $entityClass);
            throw new \Exception($message);
        }

        return $dictionary;
    }

    /**
     * @param callable $getter A callable getter that will return a value to be compared
     * @param int $flag The sort direction
     * @return self
     * @throws \Exception
     */
    public function sort(callable $getter, int $flag = SORT_ASC): self
    {
        $clone = array_map(fn($i)=>$i, $this->_container);
        $count = count($clone);
        for ($i = 0; $i < $count - 1; ++$i) {
            $swapped = false;
            for ($j = 0; $j < $count - $i - 1; ++$j) {
                if ($flag === SORT_ASC) {
                    if ($getter($clone[$j]) > $getter($clone[$j + 1])) {
                        [$clone[$j + 1], $clone[$j]] = [$clone[$j], $clone[$j + 1]];
                        $swapped = true;
                    }
                } elseif ($flag === SORT_DESC) {
                    if ($getter($clone[$j]) < $getter($clone[$j + 1])) {
                        [$clone[$j], $clone[$j + 1]] = [$clone[$j + 1], $clone[$j]];
                        $swapped = true;
                    }
                }
                if (!$swapped) {
                    break;
                }
            }
        }

        return new Collection($clone, $this->_entityClass);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->_container;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return 0;
    }
}
