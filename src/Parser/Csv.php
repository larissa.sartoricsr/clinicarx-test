<?php

declare(strict_types=1);




/**
 * Rx Avaliação PHP
 */

namespace Rx\Parser;


/**
 * Class Csv
 * Esta classe tem como objetivo converter um texto no formato CSV para array.
 *
 * @package Rx\Parser
 */
class Csv
{
    /**
     * @var string O texto original antes de ser convertido para CSV
     */
    protected $_text;

    /**
     * @var string O texto original antes de ser convertido para CSV
     */
    protected $_data;

    /**
     * CsvParser constructor.
     *
     * @param string $text O texto que será convertido para o formato CSV
     */
    public function __construct(string $text)
    {
        $this->_text = $text;
    }

    /**
     * Converte o texto no formato CSV para array
     *
     * @return array
     */
    public function toArray(): array
    {

        $lines = explode(PHP_EOL, $this->_text);
        $array = array();
        foreach ($lines as $line) {
            if ($line)
                $array[] = str_getcsv($line);
        }

        return $array;
    }
}
