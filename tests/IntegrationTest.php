<?php

declare(strict_types=1);

namespace Rx\Test;

use Cake\Log\Log;
use Cake\TestSuite\TestCase;
use Rx\Parser\Collection;
use Rx\Parser\Csv;
use Rx\Parser\Entity\Product;

class IntegrationTest extends TestCase
{
    public function testCsvConvertion()
    {
        $csvData = file_get_contents(TMP . 'frutas.csv');
        $csvParser = new Csv($csvData);

        $colection = new Collection($csvParser->toArray(), Product::class);
        $collect = $colection->toArray();
        $this->assertInstanceOf(Product::class, $collect[0]);
        $this->assertEquals(1, $collect[0]->getId());
    }
}
