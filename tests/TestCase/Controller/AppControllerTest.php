<?php
declare(strict_types=1);

namespace Rx\Test\TestCase\Controller;

use Cake\TestSuite\TestCase;
use Rx\Controller\AppController;

class AppControllerTest extends TestCase
{
    public function testInitialize()
    {
        $controller = new AppController();

        $components = $controller->components()->loaded();
        $this->assertEquals(['RequestHandler', 'Flash'], $components);
    }
}
