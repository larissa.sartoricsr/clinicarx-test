<?php

declare(strict_types=1);

namespace Rx\Test\TestCase\Parser;

use Cake\TestSuite\TestCase;
use Rx\Parser\Collection;
use Rx\Parser\Entity\Product;

class CollectionTest extends TestCase
{
    public function testGetDictionary()
    {
        $dictionary = ['id', 'name', 'description', 'value', 'quantity'];
        $this->assertEquals(
            $dictionary,
            Collection::getDictionary(Product::class)
        );
    }

    public function testCastingToArray()
    {
        $data = [
            ['id', 'name', 'description', 'value', 'quantity'],
            [1, 'Model S', 'Tesla Model S', 75000, 1],
        ];
        $collection = new Collection($data, Product::class);

        $this->assertIsArray($collection->toArray());
    }

    public function testCollectionIsCountable()
    {
        $data = [
            ['id', 'name', 'description', 'value', 'quantity'],
            [1, 'Model S', 'Tesla Model S', 75000, 1],
        ];
        $collection = new Collection($data, Product::class);

        $this->assertCount(1, $collection->toArray());
    }

    public function testCollectionIsIterable()
    {
        $data = [
            ['id', 'name', 'description', 'value', 'quantity'],
            [1, 'Model S', 'Tesla Model S', 75000, 1],
            [2, 'Model 3', 'Tesla Model 3', 50000, 1],
            [3, 'Model X', 'Tesla Model X', 35000, 1],
        ];
        $collection = new Collection($data, Product::class);

        $this->assertTrue(is_iterable($collection->toArray()));
    }

    public function testNewProductsCollectionWithHeader()
    {
        $data = [
            ['id', 'name', 'description', 'value', 'quantity'],
            [1, 'Model S', 'Tesla Model S', 75000, 1],
            [2, 'Model 3', 'Tesla Model 3', 50000, 1],
            [3, 'Model X', 'Tesla Model X', 35000, 1],
        ];

        $collection = new Collection($data, Product::class);
        $this->assertCount(3, $collection);

        foreach ($collection as $item) {
            $this->assertInstanceOf(Product::class, $item);
        }
    }

    public function testNewProductsCollectionWithoutHeader()
    {
        $data = [
            [1, 'Model S', 'Tesla Model S', 75000, 1],
            [2, 'Model 3', 'Tesla Model 3', 50000, 1],
            [3, 'Model X', 'Tesla Model X', 35000, 1],
        ];

        $collection = new Collection($data, Product::class);
        $this->assertCount(3, $collection);

        foreach ($collection as $item) {
            $this->assertInstanceOf(Product::class, $item);
        }
    }

    public function testCollectionCanBeSorted()
    {
        $data = [
            [1, 'Orange', 'Juice Orange', 5, 1],
            [2, 'Apple', 'Red Apple', 2, 1],
            [3, 'Vine', 'Green Vine', 4, 1],
            [4, 'Pear', 'Delicious Pear', 1, 1],
            [5, 'Avocado', 'Big Avocado', 3, 1],
        ];

        $collection = new Collection($data, Product::class);
        /** @var Product $item */
        $sorted = $collection->sort(fn ($item) => $item->getValue());
        $list = array_map(fn ($item) => $item->getValue(), $sorted->toArray());
        $this->assertEquals([1, 2, 3, 4, 5], $list);
    }

    public function testCollectionCanBeSortedByDesc()
    {
        $data = [
            [1, 'Orange', 'Juice Orange', 5, 1],
            [2, 'Apple', 'Red Apple', 2, 1],
            [3, 'Vine', 'Green Vine', 4, 1],
            [4, 'Pear', 'Delicious Pear', 1, 1],
            [5, 'Avocado', 'Big Avocado', 3, 1],
        ];

        $collection = new Collection($data, Product::class);
        /** @var Product $item */
        $sorted = $collection->sort(fn ($item) => $item->getValue(), SORT_DESC);
        $list = array_map(fn ($item) => $item->getValue(), $sorted->toArray());
        $this->assertEquals([5, 4, 3, 2, 1], $list);
    }

    public function testCollectionThrowsErrorWhenSortedByUnexpectedFlags()
    {
        $data = [];
        $collection = new Collection($data, Product::class);
        $this->expectException(\Exception::class);
        $collection->sort(fn ($item) => $item->getValue(), SORT_REGULAR);
    }

    public function testCollectionFailsWhenClassDoNotExists()
    {
        $this->expectException(\Exception::class);
        $collection = new Collection([], '\Rx\ClassNotExists');
    }
}
