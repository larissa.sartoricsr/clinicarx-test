<?php

declare(strict_types=1);

namespace Rx\Test\TestCase\Auth;

use Cake\TestSuite\TestCase;
use Rx\Auth\AccessControl;

class AccessControlTest extends TestCase
{
    public function testNewInstanceReturnsDefaultValue()
    {
        $access = new AccessControl();
        $default = AccessControl::ADMIN_READ | AccessControl::ADMIN_WRITE |
            AccessControl::ADMIN_EXECUTE | AccessControl::USER_READ |
            AccessControl::GUEST_READ;

        $this->assertEquals($default, $access->toInt());
    }

    public function testEnableAllAttributesShouldWork()
    {
        $access = new AccessControl();

        $access->disableAll();
        $this->assertEquals(AccessControl::NONE, $access->toInt());

        $access->enableAdminRead()
            ->enableAdminRead()
            ->enableAdminWrite()
            ->enableAdminExecute()
            ->enableUserRead()
            ->enableUserWrite()
            ->enableUserExecute()
            ->enableGuestRead()
            ->enableGuestWrite()
            ->enableGuestExecute();

        $this->assertEquals(AccessControl::ALL, $access->toInt());
    }

    public function testDisableAllAttributesShouldWork()
    {
        $access = new AccessControl();

        $access->enableAll();
        $this->assertEquals(AccessControl::ALL, $access->toInt());

        $access->disableAdminRead()
            ->disableAdminWrite()
            ->disableAdminExecute()
            ->disableUserRead()
            ->disableUserWrite()
            ->disableUserExecute()
            ->disableGuestRead()
            ->disableGuestWrite()
            ->disableGuestExecute();

        $this->assertEquals(AccessControl::NONE, $access->toInt());
    }
}
